package com.cidenet.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CidenetBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CidenetBackendApplication.class, args);
	}

}
