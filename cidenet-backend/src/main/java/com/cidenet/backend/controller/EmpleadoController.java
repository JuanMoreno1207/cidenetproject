package com.cidenet.backend.controller;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.backend.repository.EmpleadoRepository;
import com.cidenet.backend.repository.LogsRepository;
import com.cidenet.backend.entity.EmpleadoEntity;
import com.cidenet.backend.entity.LogsEntity;
import com.cidenet.backend.exceptions.EmpleadoExceptions;

@RestController
@RequestMapping("/CidenetApi")
@CrossOrigin(origins = "http://localhost:4200/")
public class EmpleadoController {
	@Autowired
	private EmpleadoRepository repositorio;
	
	@Autowired
	private LogsRepository repositoriologs;

	// Metodo para poder listar todos los empleados
	@GetMapping("/ConsultaEmpleados")
	public List<EmpleadoEntity> ConsultaEmpleados() {
		return repositorio.findAll();
	}

	// Metodo para la creacion de empleado
	@PostMapping("/GuardaEmpleado")
	public EmpleadoEntity GuardaEmpleado(@RequestBody EmpleadoEntity empleado) {
		return repositorio.save(empleado);
	}

	// Metodo para la eliminacion de empleado
	@DeleteMapping("/EliminarEmpleado/{idempleado}")
	public ResponseEntity<Map<String, Boolean>> EliminarEidmpleado(@PathVariable Long idempleado){
		EmpleadoEntity empleadoconsulta = repositorio.findById(idempleado).orElseThrow(()-> new EmpleadoExceptions("No existe el empleado "+idempleado));
		repositorio.delete(empleadoconsulta);			
		Map<String, Boolean> respuesta = new HashMap<>();
		respuesta.put("eliminar", Boolean.TRUE);
		return ResponseEntity.ok(respuesta);
	}
	
	// Metodo para consultar empleado por correo
	@GetMapping("/ConsultaPorCorreo/{Correo}")
	public  List<EmpleadoEntity> ConsultaEmpleadosPorCorrreo(@PathVariable String Correo) {
		return repositorio.findByCorreo(Correo);
	}
	
	// Metodo para encontrar el identity max en la tabla de empleados
	@GetMapping("/ConsultaIdMax")
	public BigInteger ConsultaIdMax() {
		return repositorio.Searchmax();
	}
	
	//Metodo para registrar logs dentro del software
	@PostMapping("/logs")
	public LogsEntity GuardarLog(@RequestBody LogsEntity log) {
		System.out.println(log);
		return repositoriologs.save(log);
	}

}
