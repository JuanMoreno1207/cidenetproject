package com.cidenet.backend.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "empleados")
public class EmpleadoEntity {

	public EmpleadoEntity() {

	}

	public EmpleadoEntity(Long id, String primerNombre, String otrosNombres, String primerApel, String correoc, String pais) {
		super();
		this.id = id;
		PrimerNombre = primerNombre;
		OtrosNombres = otrosNombres;
		PrimerApel = primerApel;
		correo = correoc;
		Pais = pais;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, name = "PrimerNombre", length = 20)
	private String PrimerNombre;

	@Column(nullable = true, name = "OtrosNombres", length = 50)
	private String OtrosNombres;

	@Column(nullable = false, name = "PrimerApel", length = 20)
	private String PrimerApel;

	@Column(nullable = false, name = "Correo", length = 300, unique = true)
	private String correo;

	@Column(nullable = false, name = "Pais", length = 3)
	private String Pais;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrimerNombre() {
		return PrimerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		PrimerNombre = primerNombre;
	}

	public String getOtrosNombres() {
		return OtrosNombres;
	}

	public void setOtrosNombres(String otrosNombres) {
		OtrosNombres = otrosNombres;
	}

	public String getPrimerApel() {
		return PrimerApel;
	}

	public void setPrimerApel(String primerApel) {
		PrimerApel = primerApel;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correos) {
		correo = correos;
	}

	public String getPais() {
		return Pais;
	}

	public void setPais(String pais) {
		Pais = pais;
	}

}
