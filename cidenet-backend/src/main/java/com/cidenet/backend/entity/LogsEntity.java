package com.cidenet.backend.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "logs")
public class LogsEntity {

	public LogsEntity() {

	}

	public LogsEntity(Long id, String message, String [] additional, String level, String timestamp, String fileName,
			String lineNumber) {
		super();
		this.id = id;
		this.message = message;
		this.timestamp = timestamp;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = true, name = "message", length = 300)
	private String message;

	@Column(nullable = true, name = "timestamp", length = 255)
	private String timestamp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
}
