package com.cidenet.backend.repository;
import java.math.BigInteger;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cidenet.backend.entity.EmpleadoEntity;

@Repository
public interface EmpleadoRepository extends JpaRepository<EmpleadoEntity, Long>, CrudRepository<EmpleadoEntity, Long> {
	
	List<EmpleadoEntity> findByCorreo(String correo);
	
	@Query(value = "select max(id)+1 as maxid from empleados", nativeQuery = true)
	public BigInteger Searchmax();	

}
