package com.cidenet.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cidenet.backend.entity.LogsEntity;

public interface LogsRepository extends JpaRepository<LogsEntity, Long>{
	
	

}
